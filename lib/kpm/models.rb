$:.unshift(File.dirname(__FILE__))
module Kpm
  module Model
    autoload :Response, "models/response"
    autoload :Pacakge, "models/package"
    autoload :Packages, "models/package"

    class Hashie < Virtus::Attribute
      def coerce(value)
        ::Hashie::Mash.new(value)
      end
    end

  end
end
