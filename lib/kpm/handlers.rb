$:.unshift(File.dirname(__FILE__))

module Kpm
  module Handlers
    autoload :Model, "handlers/json"
    autoload :Array, "handlers/json"
  end
end
