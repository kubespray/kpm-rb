module Kpm

  module Routes
    class << self
      def generate(client=Kpm)
        OpenAPI::Route.draw(client) do
          ## AUTH
          match "users/login", "kpm/handlers/model#response", :auth, via: :post, :options => {:skip_auth => true}

          ## Packages
          match "packages", "kpm/handlers/model#response", :list, via: :get
          match "packages/:name", "kpm/handlers/array#response", :info, via: :get
          match "packages/:name/:version/:type", "kpm/handlers/model#response", :get, via: :get
          match "packages/:name/:version/:type", "kpm/handlers/model#response", :delete, via: :delete
          match "packages/:name", "kpm/handlers/model#response", :push, via: :post
          match "packages/:name/:version/:type/pull", "kpm/handlers/model#response", :pull, via: :get, params: "format"
          match "packages/:name/:version/:type/generate", "kpm/handlers/model#response", :generate, via: :get
          match "packages/:name/:version/:type/generate-tar", "kpm/handlers/binary#fetch", :generate_tar, via: :get
          match "packages/:name/blobs/sha256/:digest", "kpm/handlers/binary#fetch", :blob, via: :get
          match "packages/:name/channels", "kpm/handlers/array#response", :channels, via: :get
          match "packages/:name/channels/:channel", "kpm/handlers/model#response", :channel, via: :get
          match "packages/:name/channels/:channel/:version", "kpm/handlers/model#response", :add_channel_release, via: :post
          match "packages/:name/channels/:channel/:version", "kpm/handlers/model#response", :delete_channel_release, via: :delete
          match "packages/:name/channels/:channel", "kpm/handlers/model#response", :delete_channel, via: :delete

          # CRUD CALLS
          match "GET", "kpm/handlers/model#response", :GET, via: :get
          match "DELETE", "kpm/handlers/model#response", :DELETE, via: :delete
          match "PUT", "kpm/handlers/model#response", :PUT, via: :get
          match "POST", "kpm/handlers/model#response", :POST, via: :post
        end
      end
    end
  end
end
