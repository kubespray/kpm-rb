module Kpm
  class AuthToken < OpenAPI::AuthToken

    def initialize
      super ({"expires_in" => 3600,
              "header" => "Authorization",
              "header_format" => "%s"})
    end

    def new_auth_token()
      # auth_response = Kpm.auth(body: {user: {email: Kpm.user, password: Kpm.password}}.to_json)
      # auth_response.authentication_token
      update({"token" => "no-token-required-yet"})
    end

  end
end
