require 'json'
require 'net/https'
require 'time'
require 'base64'

#require File.join(File.dirname(__FILE__), '../../response')
module Kpm
  module ClassMethods
    include OpenAPI::ClassMethods
    attr_accessor :user, :password

    def build_path(path, params=nil)
      uri = URI("/api/v1/#{path}")
      if params != nil
        uri.query = URI.encode_www_form(params)
      end
      return uri
    end

    def auth_token
      @auth_token ||= Kpm::AuthToken.new
    end
  end

  class << self
    include ClassMethods
  end

  class Client
    include ClassMethods

    def initialize(options={})
      @user = options[:user] || Kpm.user
      @password = options[:password] || Kpm.password
      @logger = options[:logger] || Kpm.logger
      @site = options[:site] || Kpm.site
      @request_timeout = options[:request_timeout] || Kpm.request_timeout || 5
      @max_retry = options[:max_retry] || Kpm.max_retry ||  2
      Kpm::Routes.generate(self)
      # init handlers
    end
  end
end
